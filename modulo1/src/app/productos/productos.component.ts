import { Component, OnInit } from '@angular/core';
import { Productos } from '../models/productos.model';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  productos: Productos[];
  count = 0;
  constructor() {
    this.productos = [];
    this.count = this.productos.length;
   }

  ngOnInit(): void {
  }

   guardar(producto: string, valor: string): boolean {
    console.log("Nuevo producto: " + producto + ", valor: " + valor);
    this.productos.push(new Productos(producto, valor));
    this.count++;
    console.log(this.productos);
    return false;
  }

  limpiar(): boolean {
    this.productos = [];
    this.count = 0;
    console.log(this.productos);
    return false;
  }

}
